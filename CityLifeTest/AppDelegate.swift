//
//  AppDelegate.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 02/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    private let style = Style.main
    private let serviceProvider = ServiceProvider()
    private lazy var appCoordinator: AppCoordinator = {
        let coordinatorFactory = GeneralAppCoordinatorFactory(style: style,
                                                              serviceProvider: serviceProvider)
        return AppCoordinator(style: style,
                              appCoordinatorFactory: coordinatorFactory)
    }()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        appCoordinator.start()
        return true
    }
}
