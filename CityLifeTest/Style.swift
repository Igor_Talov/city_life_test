//
//  Style.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 02/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

final class Style {
    // MARK: - Properties
    let assets: Assets

    // MARK: - Shared instances
    static let main: Style = {
        let assets = MainAssets()
        return Style(assets: assets)
    }()

    // MARK: - Initialization
    private init(assets: Assets) {
        self.assets = assets
    }
}
