//
//  SafelyRunUICodeExecution.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 02/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

func safelyRunUICode(_ closure: @escaping () -> Void) {
    guard Thread.isMainThread else {
        DispatchQueue.main.async(execute: closure)
        return
    }

    closure()
}
