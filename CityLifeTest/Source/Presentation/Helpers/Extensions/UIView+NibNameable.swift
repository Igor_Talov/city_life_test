//
//  UIView+NibNameable.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 02/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

extension UIView: NibNameable {
    class var nibName: String {
        return String(describing: self)
    }
}
