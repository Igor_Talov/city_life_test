//
//  MainColors.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 02/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

final class MainColors: Colors {

    lazy var primaryTextColor: UIColor = {
        return UIColor.black
    }()

    lazy var secondaryColor: UIColor = {
        return UIColor.gray
    }()
}
