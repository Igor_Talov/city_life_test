//
//  MainImages.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 02/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

final class MainImages: Images {
    lazy var sortIcon: UIImage = {
        return #imageLiteral(resourceName: "sort.pdf")
    }()
}
