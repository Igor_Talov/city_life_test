//
//  Assets.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 02/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

protocol Assets {
    var colors: Colors { get }
    var fonts: Fonts { get }
    var images: Images { get }
}
