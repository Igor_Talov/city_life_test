//
//  ContactsListInteractor.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 02/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

class ContactsListInteractor {
    // MARK: - Private properties
    private let contactsService: ContactsService

    // MARK: - Public properties
    weak var output: ContactsListInteractorOutput?

    // MARK: - Initialization
    init(contactsService: ContactsService) {
        self.contactsService = contactsService
    }
}

extension ContactsListInteractor: ContactsListInteractorInput {
    func fetchContacts() {
        self.contactsService.getContacts { [weak self] contacts, error in
            guard let self = self else {
                return
            }
            self.output?.didFetchContacts(contacts: contacts, with: error)
        }
    }
}
