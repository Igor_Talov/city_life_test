//
//  UserListInteractorIO.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 02/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

// MARK: - ContactsListInteractorInput
protocol ContactsListInteractorInput: class {
    func fetchContacts()
}

// MARK: - ContactsListInteractorOutput
protocol ContactsListInteractorOutput: class {
    func didFetchContacts(contacts: [ContactEntity]?, with error: Error?)
}
