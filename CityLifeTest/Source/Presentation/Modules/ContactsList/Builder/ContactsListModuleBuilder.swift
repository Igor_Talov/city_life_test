//
//  ContactsListModuleBuilder.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 02/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

class ContactsListModuleBuilder {
    // MARK: - Private properties
    private let style: Style
    private let serviceProvider: ServiceProvider

    // MARK: - Initialization
    init(style: Style,
         serviceProvider: ServiceProvider) {
        self.style = style
        self.serviceProvider = serviceProvider
    }

    func buildModule(with output: ContactsListModuleOutput) -> ModuleInterface<ContactsListModuleInput> {
        let view = makeView()
        let interactor = makeInteractor()
        let presenter = makePresenter(view: view,
                                      interactor: interactor)
        presenter.output = output
        interactor.output = presenter
        view.output = presenter
        return ModuleInterface(input: presenter, rootViewController: view)
    }
}

// MARK: - VIP factory
private extension ContactsListModuleBuilder {
    func makeView() -> ContactsListViewController {
        return ContactsListViewController(style: style)
    }

    func makePresenter(view: ContactsListViewInput,
                       interactor: ContactsListInteractorInput) -> ContactsListPresenter {
        let presenter = ContactsListPresenter(interactor: interactor,
                                              view: view)
        return presenter
    }

    func makeInteractor() -> ContactsListInteractor {
        let contactsService = serviceProvider.contactsService
        return ContactsListInteractor(contactsService: contactsService)
    }
}
