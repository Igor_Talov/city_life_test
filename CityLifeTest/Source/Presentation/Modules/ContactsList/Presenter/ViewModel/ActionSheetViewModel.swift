//
//  ActionSheetViewModel.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 03/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

struct ActionSheetViewModel {
    let alphabeticalActionTitle: String
    let reverseSortActionTitle: String
    let cancelActionTitle: String
    let cancelAction: (() -> Void)?
    let alphabeticalAction: (() -> Void)?
    let reverseSortAction: (() -> Void)?
}
