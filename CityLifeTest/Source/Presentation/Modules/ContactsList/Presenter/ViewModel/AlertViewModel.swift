//
//  AlertViewModel.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 04/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

struct AlertViewModel {
    let title: String?
    let text: String?
    let okButtonTitle: String?
    let cancelButtonTitle: String?
    let okAction: (() -> Void)?
    let cancelAction: (() -> Void)?
}
