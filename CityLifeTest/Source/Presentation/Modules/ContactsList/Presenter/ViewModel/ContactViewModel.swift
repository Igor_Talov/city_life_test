//
//  ContactViewModel.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 02/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

struct ContactViewModel {
    let fullName: String
    let email: String
    let tapAction: (() -> Void)?
}
