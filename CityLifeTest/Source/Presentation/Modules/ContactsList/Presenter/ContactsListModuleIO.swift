//
//  ContactsListModuleIO.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 02/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

// MARK: - ContactsListModuleInput
protocol ContactsListModuleInput: class {}

// MARK: - ContactsListModuleOutput
protocol ContactsListModuleOutput: class {
    func didRequestPresentContactDeitails(contact: ContactEntity)
}
