//
//  ContactsListPresenter.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 02/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

private struct State {
    var contacts: [ContactEntity]?
}

class ContactsListPresenter {
    // MARK: - Private properties
    private let interactor: ContactsListInteractorInput
    private weak var view: ContactsListViewInput?
    private var state = State()

    // MARK: - Public properties
    weak var output: ContactsListModuleOutput?

    // MARK: - Initialization
    init(interactor: ContactsListInteractorInput,
         view: ContactsListViewInput) {
        self.interactor = interactor
        self.view = view
    }
}

extension ContactsListPresenter: ContactsListModuleInput {}

extension ContactsListPresenter: ContactsListViewOutput {
    func didTapSortButton() {
        let actionSheetViewModel = makeActionSheetViewModel()
        view?.displayActionSheet(viewModel: actionSheetViewModel)
    }

    func viewDidLoad() {
        fetchContacts()
    }
}

extension ContactsListPresenter: ContactsListInteractorOutput {
    func didFetchContacts(contacts: [ContactEntity]?, with error: Error?) {
        if let error = error {
            displayAlert(error: error)
        }
        guard let contacts = contacts else {
            return
        }
        state.contacts = contacts
        let viewModels = contacts.map { self.makeContactViewModel(contactEntity: $0)}
        view?.hideLoadingView()
        view?.displayContacts(viewModels: viewModels)
    }
}

private extension ContactsListPresenter {

    func displayAlert(error: Error) {
        let cancelAction = { [weak self] in
            guard let self = self else {
                return
            }
            self.view?.hideAlert()
        }
        let alertViewModel = makeAlertViewModel(error,
                                                okAction: fetchContacts,
                                                cancelAction: cancelAction)
        view?.hideLoadingView()
        view?.displayError(viewModel: alertViewModel)
    }

    func makeContactViewModel(contactEntity: ContactEntity) -> ContactViewModel {
        let fullName = contactEntity.name
        let email = contactEntity.email ?? ""
        let tapAction = { [weak self] in
            guard let self = self else {
                return
            }
            self.didChooseContact(contactEntity: contactEntity)
        }
        return ContactViewModel(fullName: fullName, email: email, tapAction: tapAction)
    }

    func makeLoadingViewModel() -> LoadingViewModel {
        let title = NSLocalizedString("CONTACTS_LIST_LOADING_TITLE", comment: "")
        return LoadingViewModel(title: title)
    }

    func makeAlertViewModel(_ error: Error, okAction: (() -> Void)?, cancelAction: (() -> Void)?) -> AlertViewModel {
        let alertText = error.localizedDescription
        let errorTitle = NSLocalizedString("ERROR", comment: "Error alert title")
        let viewModel = AlertViewModel(title: errorTitle,
                                       text: alertText,
                                       okButtonTitle: NSLocalizedString("RETRY", comment: ""),
                                       cancelButtonTitle: NSLocalizedString("CANCEL", comment: ""),
                                       okAction: okAction,
                                       cancelAction: cancelAction)
        return viewModel
    }

    func makeActionSheetViewModel() -> ActionSheetViewModel {
        let alphabeticalActionTitle = NSLocalizedString("CONTACTS_LIST_FORWARD_SORT",
                                                        comment: "")
        let reverseSortActionTitle = NSLocalizedString("CONTACTS_LIST_REVERSE_SORT",
                                                       comment: "")
        let cancelTitle = NSLocalizedString("CANCEL", comment: "")

        let alphabeticalAction = { [weak self] in
            guard let self = self else {
                return
            }
            self.sortContactsByAlphabetical()
        }

        let reverseSortAction = { [weak self] in
            guard let self = self else {
                return
            }
            self.sortContactsByReverse()
        }

        let cancelAction = { [weak self] in
            guard let self = self else {
                return
            }
            self.view?.hideAlert()
        }

        let viewModel = ActionSheetViewModel(alphabeticalActionTitle: alphabeticalActionTitle,
                                             reverseSortActionTitle: reverseSortActionTitle,
                                             cancelActionTitle: cancelTitle,
                                             cancelAction: cancelAction,
                                             alphabeticalAction: alphabeticalAction,
                                             reverseSortAction: reverseSortAction)
        return viewModel
    }

    func didChooseContact(contactEntity: ContactEntity) {
        output?.didRequestPresentContactDeitails(contact: contactEntity)
    }

    func sortContactsByAlphabetical() {
        guard let contacts = state.contacts else {
            return
        }
        let sortedContacts = contacts.sorted { $0.name < $1.name }
        let viewModels = sortedContacts.map { self.makeContactViewModel(contactEntity: $0) }
        view?.displayContacts(viewModels: viewModels)
    }

    func sortContactsByReverse() {
        guard let contacts = state.contacts else {
            return
        }
        let sortedContacts = contacts.sorted { $0.name > $1.name }
        let viewModels = sortedContacts.map { self.makeContactViewModel(contactEntity: $0) }
        view?.displayContacts(viewModels: viewModels)
    }

    func fetchContacts() {
        let loadingViewModel = makeLoadingViewModel()
        view?.displayLoadingView(viewModel: loadingViewModel)
        interactor.fetchContacts()
    }
}
