//
//  GeneralUserListCoordinator.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 02/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

class GeneralContactsListCoordinator: ContactsListCoordinator {
    // MARK: - Private properties
    private let style: Style
    private let moduleFactory: ContactsListModuleFactory
    private let coordinatorFactory: ContactsListCoordinatorFactory
    private var childCoordinators: [Coordinator] = []
    private var rootNavigationController: UINavigationController?
    private var isStarted = false

    // MARK: - Initialization
    init(style: Style,
         moduleFactory: ContactsListModuleFactory,
         coordinatorFactory: ContactsListCoordinatorFactory) {
        self.style = style
        self.moduleFactory = moduleFactory
        self.coordinatorFactory = coordinatorFactory
    }

    func start(on rootController: UINavigationController) {
        guard !isStarted else {
            assertionFailure("\(self) is already started")
            return
        }

        isStarted = true
        rootNavigationController = rootController

        let module = moduleFactory.makeContactsListModule(output: self)
        let controller = module.rootViewController
        rootController.setViewControllers([controller], animated: false)
    }
}

extension GeneralContactsListCoordinator: ContactsListModuleOutput {
    func didRequestPresentContactDeitails(contact: ContactEntity) {
        let coordinator = coordinatorFactory.makeContactDetailsCoordinator()

        let onDismissRequest = { [weak self] in
            guard let self = self else {
                return
            }
            self.handleDismissContactDetailsCoordinator()
        }
        coordinator.onBeingDismissed = onDismissRequest
        childCoordinators.append(coordinator)
        let controller = coordinator.start(with: contact)
        rootNavigationController?.pushViewController(controller, animated: true)
    }
}

private extension GeneralContactsListCoordinator {
    func handleDismissContactDetailsCoordinator() {
        childCoordinators.removeAll()
    }
}
