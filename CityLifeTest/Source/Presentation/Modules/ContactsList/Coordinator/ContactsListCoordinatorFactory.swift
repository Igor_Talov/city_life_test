//
//  ContactsListCoordinatorFactory.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 03/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

protocol ContactsListCoordinatorFactory {
    func makeContactDetailsCoordinator() -> ContactDetailsCoordinator
}
