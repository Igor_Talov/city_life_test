//
//  GeneralContactsListCoordinatorFactory.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 03/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

class GeneralContactsListCoordinatorFactory: ContactsListCoordinatorFactory {
    // MARK: - Private properties
    private let style: Style

    // MARK: - Initialization
    init(style: Style) {
        self.style = style
    }

    func makeContactDetailsCoordinator() -> ContactDetailsCoordinator {
        let moduleFactory = GeneralContactDetailsModuleFactory(style: style)
        let coordinator = GeneralContactDetailsCoordinator(style: style,
                                                           moduleFactory: moduleFactory)
        return coordinator
    }
}
