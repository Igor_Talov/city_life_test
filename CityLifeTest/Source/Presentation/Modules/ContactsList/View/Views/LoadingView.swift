//
//  LoadingView.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 04/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

class LoadingView: UIView {
    // MARK: - Public properties
    var style: Style? {
        didSet {
            updateElementsStyle()
        }
    }

    var viewModel: LoadingViewModel? {
        didSet {
            bindViewModel(viewModel: viewModel)
        }
    }

    // MARK: - Outlets
    @IBOutlet private weak var titleLabel: UILabel?
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView?

    override func awakeFromNib() {
        super.awakeFromNib()
        updateElementsStyle()
    }

    func startAnimating() {
        activityIndicator?.startAnimating()
    }

    func stopAnimating() {
        activityIndicator?.stopAnimating()
    }
}

private extension LoadingView {
    func updateElementsStyle() {
        updateTitleLabelStyle()
    }

    func updateTitleLabelStyle() {
        titleLabel?.textColor = style?.assets.colors.secondaryColor
    }

    func bindViewModel(viewModel: LoadingViewModel?) {
        guard let viewModel = viewModel else {
            return
        }
        titleLabel?.text = viewModel.title
    }
}
