//
//  ContactViewCell.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 02/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

class ContactViewCell: UITableViewCell {
    // MARK: - Public properties
    var style: Style? {
        didSet {
            updateElementsStyle()
        }
    }

    var viewModel: ContactViewModel? {
        didSet {
            displayViewModel(viewModel: viewModel)
        }
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension ContactViewCell {
    func updateElementsStyle() {
        updateTitleLabelStyle()
        updateSubtitleLabelStyle()
    }

    func updateTitleLabelStyle() {
        textLabel?.textColor = style?.assets.colors.primaryTextColor
    }

    func updateSubtitleLabelStyle() {
        detailTextLabel?.textColor = style?.assets.colors.secondaryColor
    }

    func displayViewModel(viewModel: ContactViewModel?) {
        guard let viewModel = viewModel else {
            return
        }

        textLabel?.text = viewModel.fullName
        detailTextLabel?.text = viewModel.email
    }
}
