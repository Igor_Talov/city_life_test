//
//  ContactsListViewIO.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 02/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

// MARK: - ContactsListViewInput
protocol ContactsListViewInput: class {
    func displayContacts(viewModels: [ContactViewModel])
    func displayActionSheet(viewModel: ActionSheetViewModel)
    func displayError(viewModel: AlertViewModel)
    func displayLoadingView(viewModel: LoadingViewModel)
    func hideLoadingView()
    func hideAlert()
}

// MARK: - ContactsListViewOutput
protocol ContactsListViewOutput: class {
    func viewDidLoad()
    func didTapSortButton()
}
