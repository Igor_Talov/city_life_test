//
//  ContactsListViewController.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 02/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

private struct Constants {
    static let cellIdentifier = "Cell"
}

class ContactsListViewController: UIViewController {
    // MARK: - Private properties
    private let style: Style
    private var viewModels: [ContactViewModel] = []
    private lazy var loadingView: LoadingView = {
        let view: LoadingView = LoadingView.initFromNib()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    // MARK: - Public properties
    var output: ContactsListViewOutput?

    // MARK: - Outlets
    @IBOutlet private weak var tableView: UITableView?

    // MARK: - Initialization
    init(style: Style) {
        self.style = style
        super.init(nibName: String(describing: type(of: self)), bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        configureNavigationBar()
        setupLoadingView()
        output?.viewDidLoad()

        navigationItem.title = NSLocalizedString("CONTACTS_LIST_TITLE", comment: "")
    }
}

private extension ContactsListViewController {
    func configureTableView() {
        tableView?.register(ContactViewCell.self,
                            forCellReuseIdentifier: Constants.cellIdentifier)
        tableView?.dataSource = self
        tableView?.delegate = self
    }

    func setupLoadingView() {
        self.view.addSubview(loadingView)
        loadingView.style = style
        NSLayoutConstraint.activate([
            loadingView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            loadingView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            loadingView.topAnchor.constraint(equalTo: view.topAnchor),
            loadingView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        loadingView.isHidden = true
    }

    func configureNavigationBar() {
        let rightBarButtonItem = UIBarButtonItem(image: style.assets.images.sortIcon,
                                                 style: .plain,
                                                 target: self,
                                                 action: #selector(didTapSortButton))
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
    }
}

private extension ContactsListViewController {
    @objc
    func didTapSortButton() {
        output?.didTapSortButton()
    }
}

extension ContactsListViewController: ContactsListViewInput {
    func hideAlert() {
        safelyRunUICode {
            self.dismiss(animated: true, completion: nil)
        }
    }

    func displayError(viewModel: AlertViewModel) {
        safelyRunUICode {
            let alertController = UIAlertController(title: viewModel.title,
                                                    message: viewModel.text,
                                                    preferredStyle: .alert)
            if viewModel.okButtonTitle != nil,
                viewModel.okAction != nil {
                let okAlertAction = UIAlertAction(title: viewModel.okButtonTitle,
                                                  style: .default) { _ in
                                                    viewModel.okAction?()
                }
                alertController.addAction(okAlertAction)
            }

            if viewModel.cancelButtonTitle != nil,
                viewModel.cancelAction != nil {
                let cancelAlertAction = UIAlertAction(title: viewModel.cancelButtonTitle,
                                                      style: .cancel) { _ in
                                                        viewModel.cancelAction?()
                }
                alertController.addAction(cancelAlertAction)
            }

            self.present(alertController,
                         animated: true,
                         completion: nil)
        }
    }

    func displayLoadingView(viewModel: LoadingViewModel) {
        safelyRunUICode {
            self.loadingView.viewModel = viewModel
            self.loadingView.isHidden = false
            self.loadingView.startAnimating()
        }
    }

    func hideLoadingView() {
        safelyRunUICode {
            self.loadingView.isHidden = true
        }
    }

    func displayActionSheet(viewModel: ActionSheetViewModel) {
        safelyRunUICode {
            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            let azAction = UIAlertAction(title: viewModel.alphabeticalActionTitle,
                                         style: .default,
                                         handler: { _ in
                                            viewModel.alphabeticalAction?()
            })
            let zaAction = UIAlertAction(title: viewModel.reverseSortActionTitle,
                                         style: .default,
                                         handler: { _ in
                                            viewModel.reverseSortAction?()
            })
            let cancelAction = UIAlertAction(title: viewModel.cancelActionTitle,
                                             style: .cancel,
                                             handler: { _ in
                                                viewModel.cancelAction?()
            })
            alertController.addAction(azAction)
            alertController.addAction(zaAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }

    func displayContacts(viewModels: [ContactViewModel]) {
        safelyRunUICode {
            self.viewModels = viewModels
            self.tableView?.reloadData()
        }
    }
}

extension ContactsListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModels.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.cellIdentifier,
                                                       for: indexPath) as? ContactViewCell else {
                                                        return UITableViewCell()
        }

        let viewModel = viewModels[indexPath.row]

        cell.style = style
        cell.viewModel = viewModel
        return cell
    }
}

extension ContactsListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let cell = tableView.cellForRow(at: indexPath) as? ContactViewCell else {
            return
        }
        cell.viewModel?.tapAction?()
    }
}
