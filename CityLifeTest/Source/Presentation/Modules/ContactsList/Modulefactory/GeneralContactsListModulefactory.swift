//
//  GeneralContactsListModulefactory.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 02/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

class GeneralContactsListModuleFactory {
    // MARK: - Private properties
    private let style: Style
    private let serviceProvider: ServiceProvider

    // MARK: - Initialization
    init(style: Style,
         serviceProvider: ServiceProvider) {
        self.style = style
        self.serviceProvider = serviceProvider
    }
}

extension GeneralContactsListModuleFactory: ContactsListModuleFactory {
    func makeContactsListModule(output: ContactsListModuleOutput) -> ModuleInterface<ContactsListModuleInput> {
        let builder = ContactsListModuleBuilder(style: style,
                                                serviceProvider: serviceProvider)
        return builder.buildModule(with: output)
    }
}
