//
//  ModuleInterface.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 02/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

protocol ModuleView: class {}

struct Module<ModuleInput> {
    let input: ModuleInput
    let view: ModuleView
}

struct ModuleInterface<ModuleInput> {
    let input: ModuleInput
    let rootViewController: UIViewController
}
