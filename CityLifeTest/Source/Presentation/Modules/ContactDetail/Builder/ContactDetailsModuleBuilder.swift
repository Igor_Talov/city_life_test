//
//  ContactDetailsModuleBuilder.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 03/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

class ContactDetailsModuleBuilder {
    // MARK: - Private properties
    private let style: Style

    // MARK: - Initialization
    init(style: Style) {
        self.style = style
    }

    func buildModule(with output: ContactDetailsModuleOutput) -> ModuleInterface<ContactDetailsModuleInput> {
        let view = makeView()
        let presenter = makePresenter(view: view)
        presenter.output = output
        view.output = presenter
        return ModuleInterface(input: presenter, rootViewController: view)
    }
}

// MARK: - VIP factory
private extension ContactDetailsModuleBuilder {
    func makeView() -> ContactDetailsViewController {
        return ContactDetailsViewController(style: style)
    }

    func makePresenter(view: ContactDetailsViewInput) -> ContactDetailsPresenter {
        let presenter = ContactDetailsPresenter(view: view)
        return presenter
    }
}
