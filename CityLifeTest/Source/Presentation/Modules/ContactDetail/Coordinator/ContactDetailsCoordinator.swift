//
//  ContactDetailsCoordinator.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 03/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

protocol ContactDetailsCoordinator: Coordinator {

    var onBeingDismissed: (() -> Void)? { get set }

    func start(with contact: ContactEntity) -> UIViewController
}
