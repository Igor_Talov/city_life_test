//
//  GeneralContactDetailsCoordinator.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 03/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

class GeneralContactDetailsCoordinator: ContactDetailsCoordinator {
    // MARK: - Private properties
    private let style: Style
    private let moduleFactory: ContactDetailsModuleFactory
    private var rootViewController: UIViewController?
    private var isStarted = false

    var onBeingDismissed: (() -> Void)?

    // MARK: - Initialization
    init(style: Style,
         moduleFactory: ContactDetailsModuleFactory) {
        self.style = style
        self.moduleFactory = moduleFactory
    }

    func start(with contact: ContactEntity) -> UIViewController {
        guard !isStarted else {
            assertionFailure("\(self) is already started")
            return UIViewController()
        }

        isStarted = true
        let module = moduleFactory.makeContactDetailsModule(output: self)
        module.input.start(with: contact)
        let controller = module.rootViewController

        rootViewController = controller
        return controller
    }
}

extension GeneralContactDetailsCoordinator: ContactDetailsModuleOutput {
    func isBeingDismissed(_ contactDetailsModule: ContactDetailsModuleInput) {
        onBeingDismissed?()
    }
}
