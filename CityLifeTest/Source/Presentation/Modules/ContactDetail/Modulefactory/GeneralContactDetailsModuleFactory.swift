//
//  GeneralContactDetailsModuleFactory.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 03/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

class GeneralContactDetailsModuleFactory {
    // MARK: - Private properties
    private let style: Style

    // MARK: - Initialization
    init(style: Style) {
        self.style = style
    }
}

extension GeneralContactDetailsModuleFactory: ContactDetailsModuleFactory {
    func makeContactDetailsModule(output: ContactDetailsModuleOutput) -> ModuleInterface<ContactDetailsModuleInput> {
        let builder = ContactDetailsModuleBuilder(style: style)
        return builder.buildModule(with: output)
    }
}
