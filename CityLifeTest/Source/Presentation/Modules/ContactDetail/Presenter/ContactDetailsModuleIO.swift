//
//  ContactDetailsModuleIO.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 03/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

// MARK: - ContactDetailsModuleInput
protocol ContactDetailsModuleInput: class {
    func start(with contact: ContactEntity)
}

// MARK: - ContactDetailsModuleOutput
protocol ContactDetailsModuleOutput: class {
    func isBeingDismissed(_ contactDetailsModule: ContactDetailsModuleInput)
}
