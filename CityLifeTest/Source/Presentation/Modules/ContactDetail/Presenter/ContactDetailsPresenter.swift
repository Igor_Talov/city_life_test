//
//  ContactDetailsPresenter.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 03/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

private struct State {
    var contact: ContactEntity?
}

class ContactDetailsPresenter {
    // MARK: - Private properties
    private weak var view: ContactDetailsViewInput?
    private var state = State()

    // MARK: - Public properties
    weak var output: ContactDetailsModuleOutput?

    // MARK: - Initialization
    init(view: ContactDetailsViewInput) {
        self.view = view
    }
}

extension ContactDetailsPresenter: ContactDetailsViewOutput {
    func isBeingDismissed() {
        output?.isBeingDismissed(self)
    }

    func viewDidLoad() {
        guard let contact = state.contact else {
            assertionFailure("Contact is nil")
            return
        }
        let viewModels = makeViewModels(for: contact)
        view?.displayDetails(viewModels: viewModels)
    }
}

extension ContactDetailsPresenter: ContactDetailsModuleInput {
    func start(with contact: ContactEntity) {
        state.contact = contact
    }
}

private extension ContactDetailsPresenter {
    func makeViewModels(for contact: ContactEntity) -> [ContactDetailViewModel] {
        var viewModels: [ContactDetailViewModel] = []
        let nameViewModel = ContactDetailViewModel(title: contact.username,
                                                   subtitle: "Username")
        viewModels.append(nameViewModel)
        if let phone = contact.phone {
            let phoneViewModel = ContactDetailViewModel(title: phone,
                                                        subtitle: "Phone")
            viewModels.append(phoneViewModel)
        }

        let addressViewModel = ContactDetailViewModel(title: contact.address.street,
                                                      subtitle: "Address")
        viewModels.append(addressViewModel)

        if let website = contact.website {
            let websiteViewModel = ContactDetailViewModel(title: website,
                                                          subtitle: "WebSite")
            viewModels.append(websiteViewModel)
        }

        let companyViewModel = ContactDetailViewModel(title: contact.company.name,
                                                      subtitle: "Company")
        viewModels.append(companyViewModel)
        return viewModels
    }
}
