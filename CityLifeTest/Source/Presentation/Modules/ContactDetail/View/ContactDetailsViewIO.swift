//
//  ContactDetailsViewIO.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 03/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

// MARK: - ContactDetailsViewInput
protocol ContactDetailsViewInput: class {
    func displayDetails(viewModels: [ContactDetailViewModel])
}

// MARK: - ContactDetailsViewOutput
protocol ContactDetailsViewOutput: class {
    func viewDidLoad()
    func isBeingDismissed()

}
