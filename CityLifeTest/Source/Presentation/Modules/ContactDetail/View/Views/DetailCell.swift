//
//  DetailCell.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 03/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

class DetailCell: UITableViewCell {
    // MARK: - Public properties
    var style: Style? {
        didSet {
            updateElementsStyle()
        }
    }

    var viewModel: ContactDetailViewModel? {
        didSet {
            displayViewModel(viewModel: viewModel)
        }
    }

    // MARK: - Initialization
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension DetailCell {
    func updateElementsStyle() {
        updateTitleLabelStyle()
        updateSubtitleLabelStyle()
    }

    func updateTitleLabelStyle() {
        textLabel?.textColor = style?.assets.colors.primaryTextColor
    }

    func updateSubtitleLabelStyle() {
        detailTextLabel?.textColor = style?.assets.colors.secondaryColor
    }

    func displayViewModel(viewModel: ContactDetailViewModel?) {
        guard let viewModel = viewModel else {
            return
        }

        textLabel?.text = viewModel.title
        detailTextLabel?.text = viewModel.subtitle
    }
}
