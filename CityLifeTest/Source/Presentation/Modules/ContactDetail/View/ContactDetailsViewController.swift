//
//  ContactDetailsViewController.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 03/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

private struct Constants {
    static let cellIdentifier = "Cell"
}

class ContactDetailsViewController: UIViewController {
    // MARK: - Private properties
    private let style: Style
    private var viewModels: [ContactDetailViewModel] = []

    // MARK: - Public properties
    var output: ContactDetailsViewOutput?

    // MARK: - Outlets
    @IBOutlet private weak var tableView: UITableView?

    // MARK: - Initialization
    init(style: Style) {
        self.style = style
        super.init(nibName: String(describing: type(of: self)), bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        output?.viewDidLoad()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        if isMovingFromParent || isBeingDismissed {
            output?.isBeingDismissed()
        }
    }
}

private extension ContactDetailsViewController {
    func configureTableView() {
        tableView?.register(DetailCell.self,
                            forCellReuseIdentifier: Constants.cellIdentifier)
        tableView?.dataSource = self
    }
}

extension ContactDetailsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModels.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.cellIdentifier,
                                                       for: indexPath) as? DetailCell else {
                                                        return UITableViewCell()
        }

        let viewModel = viewModels[indexPath.row]

        cell.style = style
        cell.viewModel = viewModel
        return cell
    }
}

extension ContactDetailsViewController: ContactDetailsViewInput {
    func displayDetails(viewModels: [ContactDetailViewModel]) {
        safelyRunUICode {
            self.viewModels = viewModels
            self.tableView?.reloadData()
        }
    }
}
