//
//  ServiceProvider.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 04/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

class ServiceProvider {
    // MARK: - Public properties
    private(set) lazy var contactsService: ContactsService = {
        let service = GeneralContactsService()
        return service
    }()
}
