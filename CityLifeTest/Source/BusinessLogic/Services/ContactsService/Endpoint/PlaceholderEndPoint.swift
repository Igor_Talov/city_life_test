//
//  PlaceholderEndPoint.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 03/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

//enum NetworkingEnvironment {
//    case qaenviroment
//    case production
//    case staging
//}

enum JSONPlaceholderAPI {
    case getContacts
}

extension JSONPlaceholderAPI: EndPointType {

    var environmentBaseURLString: String {
        return "http://jsonplaceholder.typicode.com"
    }

    var baseURLString: String {
        return environmentBaseURLString + path
    }

    var path: String {
        switch self {
        case .getContacts:
            return "/users"
        }
    }

    var httpMethod: HTTPMethod {
        return .get
    }

    var task: HTTPTask {
        return .request
    }

    var headers: HTTPHeaders? {
        return nil
    }
}
