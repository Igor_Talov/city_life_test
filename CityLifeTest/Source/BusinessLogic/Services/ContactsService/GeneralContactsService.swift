//
//  GeneralContactsService.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 03/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

class GeneralContactsService {
    // MARK: - Private properties
    private lazy var workQueue: OperationQueue = {
        let queue = OperationQueue()
        queue.name = "com.SealDev.contactsService"
        return queue
    }()

    let router = Router<JSONPlaceholderAPI>()
}

extension GeneralContactsService: ContactsService {
    func getContacts(completion: @escaping ([ContactEntity]?, Error?) -> Void) {
        performGetContacts(completion: completion)
    }
}

private extension GeneralContactsService {
    func performGetContacts(completion: @escaping ([ContactEntity]?, Error?) -> Void) {
        workQueue.addOperation {
            self.router.request(.getContacts, completion: { data, _, error in
                if let error = error {
                    completion(nil, error)
                    return
                }
                guard let data = data else {
                    return
                }
                do {
                    let contacts = try JSONDecoder().decode([ContactEntity].self, from: data)
                    completion(contacts, nil)
                } catch {
                    print(error)
                }
            })
        }
    }
}
