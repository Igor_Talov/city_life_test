//
//  ParameterEncoding.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 03/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

typealias Parameters = [String: Any]

protocol ParameterEncoder {
    static func encode(urlRequest: inout URLRequest, with parameters: Parameters) throws
}

enum NetworkError: String, Error {
    case parameterNil = "Parameters were nil."
    case encodingFailed = "Parameter encoding fail"
    case missingURL = "URL is nil"
}
