//
//  HTTPTask.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 03/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

typealias HTTPHeaders = [String: String]

enum HTTPTask {
    case request
    case requestParameter(bodyParameters: Parameters?,
        urlParameters: Parameters?)
    case requestParametersAndHeaders(bodyParameters: Parameters?,
        urlParameters: Parameters?, additionalHeaders: HTTPHeaders?)
}
