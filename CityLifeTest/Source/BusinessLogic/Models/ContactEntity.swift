//
//  ContactEntity.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 02/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

// MARK: - ContactEntity
struct ContactEntity: Decodable {
    let id: Int
    let name: String
    let username: String
    let email: String?
    let address: Address
    let phone: String?
    let website: String?
    let company: Company
}

// MARK: - Address
struct Address: Codable {
    let street: String
    let suite: String
    let city: String
    let zipcode: String
    let geo: Geo
}

// MARK: - Geo
struct Geo: Codable {
    let lat: String
    let lng: String
}

// MARK: - Company
struct Company: Codable {
    let name: String
    let catchPhrase: String
    let bs: String
}
