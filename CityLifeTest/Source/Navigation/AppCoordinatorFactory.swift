//
//  AppCoordinatorFactory.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 02/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

protocol AppCoordinatorFactory {
    func makeContactsListCoordinator() -> ContactsListCoordinator
}
