//
//  AppCoordinator.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 02/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

class AppCoordinator: Coordinator {
    // MARK: - Private properties
    private let appCoordinatorFactory: AppCoordinatorFactory
    private let style: Style

    private var window: UIWindow?
    private lazy var rootController = UINavigationController()
    private var childCoordinators: [Coordinator] = []
    private var isStarted = false

    // MARK: - Initializations
    init(style: Style,
         appCoordinatorFactory: AppCoordinatorFactory) {
        self.style = style
        self.appCoordinatorFactory = appCoordinatorFactory
    }

    func start() {
        guard !isStarted else {
            assertionFailure("\(self) is already started")
            return
        }

        isStarted = true
        let screenBounds = UIScreen.main.bounds
        window = UIWindow(frame: screenBounds)
        window?.rootViewController = rootController
        window?.makeKeyAndVisible()
        setupContactsViewController()
    }
}

private extension AppCoordinator {
    func setupContactsViewController() {
        let coordinator = appCoordinatorFactory.makeContactsListCoordinator()
        childCoordinators.append(coordinator)
        coordinator.start(on: rootController)
    }
}
