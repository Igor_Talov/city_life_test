//
//  GeneralAppCoordinatorFactory.swift
//  CityLifeTest
//
//  Created by Игорь Талов on 02/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

class GeneralAppCoordinatorFactory {
    // MARK: - Private properties
    private let style: Style
    private let serviceProvider: ServiceProvider

    // MARK: - Initialization
    init(style: Style,
         serviceProvider: ServiceProvider) {
        self.style = style
        self.serviceProvider = serviceProvider
    }

    func makeContactsListCoordinator() -> ContactsListCoordinator {
        let moduleFactory = GeneralContactsListModuleFactory(style: style,
                                                             serviceProvider: serviceProvider)
        let coordinatorFactory = GeneralContactsListCoordinatorFactory(style: style)
        let coordinator = GeneralContactsListCoordinator(style: style,
                                                         moduleFactory: moduleFactory,
                                                         coordinatorFactory: coordinatorFactory)
        return coordinator
    }
}

extension GeneralAppCoordinatorFactory: AppCoordinatorFactory {}
